import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        title: [],
        description: [],
    },
    getters: {
        getTitle(state) {
            return state.title;
        },
        getDescription(state) {
            return state.description;
        },
    },
    mutations: {
        saveTitle(state, titleData) {
            if (localStorage.titles) {
                console.log("k");
                console.log(localStorage.titles);
                state.title.push(localStorage.getItem('titles').split(','));
                state.title.push(titleData);
                console.log(state.title);
                localStorage.removeItem('titles');
                localStorage.setItem('titles', state.title);
                console.log(state.title);
                
            } else {
                state.title.push(titleData);
                console.log(state.title);
                localStorage.setItem('titles', state.title);
            }
        },

        saveDescriptione(state, descriptionData) {
            if (localStorage.descriptions) {
                state.description.push(
                    localStorage.getItem('descriptions').split(',')
                );
                state.description.push(descriptionData);
                console.log(state.description);
                localStorage.removeItem('descriptions');
                localStorage.setItem('descriptions', state.description);
                console.log(state.description);
                
            } else {
                state.description.push(descriptionData);
                console.log(state.description);
                localStorage.setItem('descriptions', state.description);
            }
        },

        editDescriptione(state, index) {
           
            console.log( localStorage.titles);
            state.title = localStorage.titles.split(",")
            console.log(state.title[index] );
            state.title.splice(index, 1)
            console.log(state.title); 
            
            console.log( localStorage.descriptions);
            state.description = localStorage.descriptions.split(",")
            console.log(state.description[index] );
            state.description.splice(index, 1)
            console.log(state.description); 

            localStorage.removeItem('titles');
            localStorage.setItem('titles', state.title);
            localStorage.removeItem('descriptions');
            localStorage.setItem('descriptions', state.description);
           
            console.log(localStorage.titles);
           




          



           
        },
    },
    actions: {
        saveData({ commit }) {
            commit('saveData');
        },
    },
    modules: {},
});
